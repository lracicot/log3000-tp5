const express = require('express');
const cors = require('cors');
const app = express();

const PORT = 8080;
const HOST = '0.0.0.0';

app.use(cors());

app.get('/', function (req, res) {
  res.send('Access granted');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
